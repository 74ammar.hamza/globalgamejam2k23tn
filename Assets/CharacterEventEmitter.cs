using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEventEmitter : MonoBehaviour
{
    
    public void Walk1()
    {
        GameManager.Instance.Player.Walk1();
    }

    public void Walk2()
    {
        GameManager.Instance.Player.Walk2();

    }

    public void Walk3()
    {
        GameManager.Instance.Player.Walk3();
    }
    public void Walk4()
    {
        GameManager.Instance.Player.Walk4();
    }
}
