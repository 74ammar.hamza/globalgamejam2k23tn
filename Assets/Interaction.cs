using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    private GameObject pickedObject;
    public float pickUpDistance = 2f;
    private bool nearObject = false;
    public GameObject door;
    public Transform dropSpot;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Picked"))
        {
            nearObject = true;
        }
    }
    private void Update()
    {
        if (pickedObject != null && Vector3.Distance(pickedObject.transform.position, dropSpot.position) >= 0f)
        {
            door.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Picked"))
        {
            nearObject = false;
        }
    }

    private void OnGUI()
    {
        if (nearObject && pickedObject == null)
        {
            GUI.Box(new Rect(Screen.width / 2 - 50, 0, 100, 25), "Press E ");


            if (Input.GetKeyDown(KeyCode.E))
            {
                pickedObject = GameObject.FindWithTag("Picked");
                pickedObject.transform.position = transform.position + transform.forward * pickUpDistance;
                pickedObject.transform.parent = transform;
            }
        }

        if (pickedObject != null)
        {
            GUI.Box(new Rect(Screen.width / 2 - 50, 0, 100, 25), "Press C to Drop");
            if (Input.GetKeyDown(KeyCode.C))
            {
                pickedObject.transform.parent = null;


                if (Vector3.Distance(pickedObject.transform.position, dropSpot.position) < 2f)
                { 
                pickedObject.transform.position = dropSpot.position;
                    door.SetActive(false);
                }
                else
                {
                    pickedObject.transform.position = transform.position + transform.forward * pickUpDistance;
                }
               
                pickedObject = null;
            }
        }
    }
}






