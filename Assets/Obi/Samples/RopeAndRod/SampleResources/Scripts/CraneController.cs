﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class CraneController : MonoBehaviour {

	ObiRopeCursor cursor;
	ObiRope rope;

	// Use this for initialization
	void Start () {
		cursor = GetComponentInChildren<ObiRopeCursor>();
		rope = cursor.GetComponent<ObiRope>();
/*		cursor.ChangeLength(1000f);
*/	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.W)){
			if (rope.restLength > 6.5f)
				cursor.ChangeLength(rope.restLength - 1f * Time.deltaTime);
		}
		
			cursor.ChangeLength(rope.restLength + 3f * Time.deltaTime);


	}
}
