using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;
using System;
public class Roots : MonoBehaviour
{
    public static Action OnRootFinished;
    [SerializeField]
    private MeshFilter m_MeshFilter;
    private Vector3[] m_AllPoints;
    private int[] m_AllTriangles;
    private List<int> m_usedTriangles;
    [SerializeField]
    private float m_yValueOfRoot;
    public float ClippingLevel
    {
        get
        {
            return Mathf.Lerp(m_MinY, m_MaxY, ClippingPercentage / 100f);
        }
    }
    public float ClippingPercentage;
    public float GoalClippingPercentage = 30f;
    
    private Vector2 Hidden = new Vector2(0, 0);
    private Vector2 Shown = new Vector2(.5f, .5f);
    public int ShownVerts;
    public int HiddenVerts;

    [SerializeField]
    private float m_FadeSpeed = 2f;
    private float m_MaxY;
    private float m_MinY;
    private bool m_finished;
    private Mesh m_Mesh
    {
        get
        {
            return m_MeshFilter.mesh;
        }
    }
    private void OnEnable()
    {
        ClippingPercentage = GoalClippingPercentage;
        InitializeMesh();
        CalculateUsedVertices();

    }
    [Button]
    public void Grow()
    {
        //transform.localScale = transform.localScale + Vector3.one * .01f;
        GoalClippingPercentage += m_FadeSpeed * Time.deltaTime ;
        GoalClippingPercentage = Mathf.Clamp(GoalClippingPercentage, 0, 100);
    }

    private void Update()
    {
        if (ClippingPercentage > 99.9F && !m_finished)
        {
            m_finished = true;
            ClippingPercentage = 100f;
            OnRootFinished?.Invoke();
            SoundManager.Instance.PlaySoundFX(eSounds.Success);
        }
        if (GoalClippingPercentage != ClippingPercentage)
        {
            CameraManager.Instance.Shake();
            ClippingPercentage = Mathf.Lerp(ClippingPercentage, GoalClippingPercentage, 10 * Time.deltaTime);
            UpdateVertices();
        }
    }

    public void UpdateVertices()
    {
        CalculateUsedVertices();
    }

    public void CalculateUsedVertices()
    {
        HiddenVerts = 0;
        ShownVerts = 0;
        m_usedTriangles = new List<int>();
        Vector2[] uvs = new Vector2[m_Mesh.uv.Length];

        for (int i = 0; i< m_AllPoints.Length; i++)
        {
            if (m_AllPoints[i].y > ClippingLevel)
            {
                uvs[i] = Hidden;
                HiddenVerts++;
            }
            else
            {
                uvs[i] = Shown;
                ShownVerts++;
            }
        }        
        m_Mesh.uv = uvs;
        Debug.Log(HiddenVerts.ToString() + "/" +ShownVerts.ToString() + " Vertices Used");
    }

    public void InitializeMesh()
    {
        m_AllPoints = m_Mesh.vertices;
        Vector2[] uvs = new Vector2[m_Mesh.uv.Length];
        for (int i = 0; i< m_Mesh.uv.Length; i++)
        {
            if (m_MaxY< m_AllPoints[i].y)
            {
                m_MaxY = m_AllPoints[i].y;
            }
            if (m_MinY > m_AllPoints[i].y)
            {
                m_MinY = m_AllPoints[i].y;
            }
            uvs[i] = Shown;
        }
        m_Mesh.uv = uvs;
        m_AllPoints = m_Mesh.vertices;
        Debug.Log(m_AllPoints.Length+" Triangles Count");
    }


}
