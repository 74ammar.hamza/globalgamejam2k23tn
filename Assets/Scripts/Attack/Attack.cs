using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Attack : MonoBehaviour
{
    [SerializeField]
    private GameObject Prefab;
    [SerializeField]
    private Transform Posion_p;
    private GameObject Poison;
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private float force;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) )
        {
            Poison = Instantiate(Prefab.gameObject, Posion_p.position, Quaternion.identity);
            Poison.gameObject.name = "Poison";
            rb = Poison.GetComponent<Rigidbody>();
            rb.velocity = transform.forward * force;
           Destroy(Poison,2f);
        }
    }
   

}
