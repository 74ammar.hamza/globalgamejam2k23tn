
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using Obi;

public class Character : CharacterBase
{
    [ShowInInspector]
    public PlayerSettings PlayerSettings
    {
        get
        {
            return GameConfig.Instance.PlayerSettings;
        }
    }
    [SerializeField]
    private Rigidbody m_RigidBody;
    [SerializeField]
    private Collider m_Collider;
    [SerializeField]
    private Animator m_Animator;
    [SerializeField]
    private Transform m_Transform;
    [SerializeField]
    private Transform m_SkinTransform;
    [SerializeField]
    private Transform m_Grapple;
    [SerializeField]
    private ObiRope m_ObiRope;
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private Transform m_AimPoint;
    [SerializeField]
    private Transform m_GunEndPoint;
    
    public Vector3 Gravity;
    public static float globalGravity = -9.81f;
    private Vector2 _moveInput;
    public float LastPressedJumpTime { get; private set; }
    public float LastWaterForce { get; private set; }
    public float LastOnGroundTime { get; private set; }
    //Jump
    private bool _isJumpCut;
    private bool _isJumpFalling;
    private bool m_WaterPropulsionStarted;
    public bool IsJumping { get; private set; }
    [SerializeField]
    private ParticleSystem m_WaterFx;

    [SerializeField]
    private float m_WaterMaximum = 2f;
    [SerializeField]
    private float m_WaterCurrentThreshhold;

    public override void OnEnable()
    {
        base.OnEnable();
        SetGravityScale(PlayerSettings.gravityScale);
        GrapplingHook.OnGrapple += OnGrapple;
        GrapplingHook.OnRelease += OnGrappleRelease;
        m_WaterCurrentThreshhold = m_WaterMaximum;
        UIManager.Instance.SetHpValue(1f);
        UIManager.Instance.SetWaterValue(1f);
        Roots.OnRootFinished += Heal;

    }
    private void OnDisable()
    {
        GrapplingHook.OnGrapple -= OnGrapple;
        GrapplingHook.OnRelease -= OnGrappleRelease;
        Roots.OnRootFinished -= Heal;
    }

    public void Heal()
    {
        CurrentHealth += 3f;
    }
    private void Update()
    {
        #region MovementUpdate
        UIManager.Instance.SetHpValue(CurrentHealth / MaxHealth);
        UIManager.Instance.SetWaterValue(m_WaterCurrentThreshhold/m_WaterMaximum);

        m_Animator.SetFloat("XVelocity", m_RigidBody.velocity.x/10f);
        m_Animator.SetFloat("YVelocity", m_RigidBody.velocity.y/5f);
        if (_moveInput.x != 0)
        {
            m_SkinTransform.localEulerAngles = Vector3.up * (_moveInput.x > 0 ? 90f : -90f);
        }
        _moveInput.x = Input.GetAxisRaw("Horizontal");
        _moveInput.y = Input.GetAxisRaw("Vertical");
        LastOnGroundTime -= Time.deltaTime;
        LastPressedJumpTime -= Time.deltaTime;
        LastWaterForce -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnJumpInput();
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            OnJumpUpInput();
        }
        Vector3 direction = (m_Transform.position - InputManager.Instance.MouseWorldPosition).normalized;
        m_AimPoint.localPosition = -direction * 3f;
        Vector3 waterPosition = m_GunEndPoint.position;
        waterPosition.z = 0;
        m_WaterFx.transform.position = waterPosition;
        m_WaterFx.transform.right = -direction;
        if (Input.GetMouseButtonDown(1) && m_WaterCurrentThreshhold>.6f)
        {
            m_WaterPropulsionStarted = true;
        }
        if (Input.GetMouseButtonUp(1))
        {
            m_WaterPropulsionStarted = false;
        }
        if (m_WaterPropulsionStarted)
        {
            if (m_WaterCurrentThreshhold > 0)
            {
                SoundManager.Instance.PlaySoundFX(eSounds.WaterPump);
                AddWaterForce(direction);
                m_WaterFx.emissionRate = 30;
                m_WaterCurrentThreshhold -= Time.deltaTime;
                m_WaterCurrentThreshhold = Mathf.Clamp(m_WaterCurrentThreshhold, 0, m_WaterMaximum);
            }
            else
            {
                m_WaterPropulsionStarted = false;
            }

        }
        else
        {
            m_WaterCurrentThreshhold += Time.deltaTime;
            m_WaterCurrentThreshhold = Mathf.Clamp(m_WaterCurrentThreshhold, 0, m_WaterMaximum);
            m_WaterFx.emissionRate = 0f;
        }
        #region JUMP CHECKS
        if (IsJumping && m_RigidBody.velocity.y < 0)
        {
            IsJumping = false;

        }


        if (LastOnGroundTime > 0 && !IsJumping)
        {
            _isJumpCut = false;

            if (!IsJumping)
                _isJumpFalling = false;
        }
        if (!IsJumping)
        {
            GroundCheck();
        }
        if (CanJump() && LastPressedJumpTime > 0)
        {
            IsJumping = true;
            _isJumpCut = false;
            _isJumpFalling = false;
            Jump();

/*            AnimHandler.startedJumping = true;
*/        
        }
        #endregion
        #endregion

    }

    private void FixedUpdate()
    {
        Run(1);
        ApplyGravity();
    }

    #region Movement
    public void Run(float lerpAmount)
    {
        //Calculate the direction we want to move in and our desired velocity
        float targetSpeed = _moveInput.x * PlayerSettings.runMaxSpeed;
        //We can reduce are control using Lerp() this smooths changes to are direction and speed
        targetSpeed = Mathf.Lerp(m_RigidBody.velocity.x, targetSpeed, lerpAmount);

        #region Calculate AccelRate
        float accelRate;

        //Gets an acceleration value based on if we are accelerating (includes turning) 
        //or trying to decelerate (stop). As well as applying a multiplier if we're air borne.
        accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? PlayerSettings.runAccelAmount : PlayerSettings.runDeccelAmount;
        #endregion
        #region Conserve Momentum
        //We won't slow the player down if they are moving in their desired direction but at a greater speed than their maxSpeed
        if (PlayerSettings.doConserveMomentum && Mathf.Abs(m_RigidBody.velocity.x) > Mathf.Abs(targetSpeed) && Mathf.Sign(m_RigidBody.velocity.x) == Mathf.Sign(targetSpeed) && Mathf.Abs(targetSpeed) > 0.01f)
        {
            //Prevent any deceleration from happening, or in other words conserve are current momentum
            //You could experiment with allowing for the player to slightly increae their speed whilst in this "state"
            accelRate = 0;
        }
        #endregion

        //Calculate difference between current velocity and desired velocity
        float speedDif = targetSpeed  - m_RigidBody.velocity.x;
        //Calculate force along x-axis to apply to thr player
        float movement = speedDif * accelRate;

        //Convert this to a vector and apply to rigidbody
        m_RigidBody.AddForce(movement * Vector3.right, ForceMode.Acceleration);

        /*
		 * For those interested here is what AddForce() will do
		 * RB.velocity = new Vector2(RB.velocity.x + (Time.fixedDeltaTime  * speedDif * accelRate) / RB.mass, RB.velocity.y);
		 * Time.fixedDeltaTime is by default in Unity 0.02 seconds equal to 50 FixedUpdate() calls per second
		*/
        #region GRavity
        if (m_RigidBody.velocity.y < 0 && _moveInput.y < 0)
        {
            //Much higher gravity if holding down
            SetGravityScale(PlayerSettings.gravityScale * PlayerSettings.fastFallGravityMult);
            //Caps maximum fall speed, so when falling over large distances we don't accelerate to insanely high speeds
            m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, Mathf.Max(m_RigidBody.velocity.y, -PlayerSettings.maxFastFallSpeed),0f);
        }
        else if (_isJumpCut)
        {
            //Higher gravity if jump button released
            SetGravityScale(PlayerSettings.gravityScale * PlayerSettings.jumpCutGravityMult);
            m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, Mathf.Max(m_RigidBody.velocity.y, -PlayerSettings.maxFallSpeed),0f);
        }
        else if ((IsJumping || _isJumpFalling) && Mathf.Abs(m_RigidBody.velocity.y) < PlayerSettings.jumpHangTimeThreshold)
        {
            SetGravityScale(PlayerSettings.gravityScale * PlayerSettings.jumpHangGravityMult);
        }
        else if (m_RigidBody.velocity.y < 0)
        {
            //Higher gravity if falling
            SetGravityScale(PlayerSettings.gravityScale * PlayerSettings.fallGravityMult);
            //Caps maximum fall speed, so when falling over large distances we don't accelerate to insanely high speeds
            m_RigidBody.velocity = new Vector2(m_RigidBody.velocity.x, Mathf.Max(m_RigidBody.velocity.y, -PlayerSettings.maxFallSpeed));
        }
        else
        {
            //Default gravity if standing on a platform or moving upwards
            SetGravityScale(PlayerSettings.gravityScale);
        }


    #endregion
    }
    #region Jump
    public void Jump()
    {
        //Ensures we can't call Jump multiple times from one press
        LastPressedJumpTime = 0;
        LastOnGroundTime = 0;

        #region Perform Jump
        //We increase the force applied if we are falling
        //This means we'll always feel like we jump the same amount 
        //(setting the player's Y velocity to 0 beforehand will likely work the same, but I find this more elegant :D)
        float force = PlayerSettings.jumpForce;

        if (m_RigidBody.velocity.y < 0)
        {
            force -= m_RigidBody.velocity.y;
        }
        Debug.Log(force);

        m_RigidBody.AddForce(Vector3.up * force, ForceMode.Impulse);
        #endregion
    }

    public void AddWaterForce(Vector3 i_Direction)
    {
        m_RigidBody.AddForce(i_Direction * PlayerSettings.WaterForce, ForceMode.Acceleration);
        LastWaterForce = .1f;
    }

    public void GroundCheck()
    {
        if (Physics.BoxCast(m_Transform.position, PlayerSettings.playerSize, -m_Transform.up, m_Transform.rotation, PlayerSettings.maxDistance, PlayerSettings.GroundLayerMask))
        {
            if (LastOnGroundTime < -0.1f)
            {
                //AnimHandler.justLanded = true;
            }
            LastOnGroundTime = PlayerSettings.coyoteTime; //if so sets the lastGrounded to coyoteTime
        }
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position - transform.up * PlayerSettings.maxDistance, PlayerSettings.playerSize);
    }
    public void OnJumpInput()
    {
        LastPressedJumpTime = PlayerSettings.jumpInputBufferTime;
    }

    public void OnJumpUpInput()
    {
        if (CanJumpCut())
            _isJumpCut = true;
    }

    private bool CanJump()
    {
        return LastOnGroundTime > 0 && !IsJumping;
    }

    private bool CanJumpCut()
    {
        return IsJumping && m_RigidBody.velocity.y > 0;
    }


    #endregion

    public void SetGravityScale(float scale)
    {
        Gravity = Vector3.up* globalGravity * scale;
    }

    public void ApplyGravity()
    {
        //m_RigidBody.AddForce(Gravity, ForceMode.Acceleration);
    }

    #endregion
    public void OnGrapple()
    {
        Debug.Log("Grapple On");
        m_Animator.SetFloat("Grapple", 1);
    }

    public void OnGrappleRelease()
    {
        Debug.Log("Grapple Release");
        m_Animator.SetFloat("Grapple", 0);

    }
    #region Grappling

    public void HitWithWater()
    {

    }


    public void Walk1()
    {
        if (LastOnGroundTime>0)
        SoundManager.Instance.PlaySoundFX(eSounds.walk1);
    }

    public void Walk2()
    {
        if (LastOnGroundTime>0)
        SoundManager.Instance.PlaySoundFX(eSounds.walk2);

    }

    public void Walk3()
    {
        if (LastOnGroundTime>0)
        SoundManager.Instance.PlaySoundFX(eSounds.walk3);
    }
    public void Walk4()
    {
        if (LastOnGroundTime>0)
        SoundManager.Instance.PlaySoundFX(eSounds.walk4);
    }

    public override void Die()
    {
        base.Die();
        GameManager.Instance.ReloadLevel();
    }



    #endregion
}