using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBase : MonoBehaviour
{
    [Header("Character Health")]
    public float MaxHealth = 10f;
    public float CurrentHealth = 10f;

    public virtual void RecieveDamage(float i_Damage)
    {
        CurrentHealth -= i_Damage;
        if (CurrentHealth<= 0)
        {
            Die();
        }
    }

    public virtual void OnEnable()
    {
        CurrentHealth = MaxHealth;
    }
    public virtual void Die()
    {

    }
}
