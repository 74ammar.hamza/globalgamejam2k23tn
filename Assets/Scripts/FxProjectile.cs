using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxProjectile : ProjectileBase
{
    public Component m_Target;
    [SerializeField]
    private ParticleSystem m_WaterFx;
    [SerializeField]
    private ParticleSystem m_Explosion;
    public override void LaunchProjectile()
    {
        base.LaunchProjectile();
        m_WaterFx.Play();
    }

    public override void PlayExplosion()
    {
        base.PlayExplosion();
        m_Explosion.Play();
        
    }
}
