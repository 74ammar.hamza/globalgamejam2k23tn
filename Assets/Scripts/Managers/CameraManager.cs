﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using Sirenix.OdinInspector;
#endif
using DG.Tweening;

[DefaultExecutionOrder(-40)]
public class CameraManager : Singleton<CameraManager> {

    [Header("Camera State")]
    private CameraStates m_CameraState;
    public CameraStates CameraState {
        get {
            return m_CameraState;
        }
        set {
            if (value != m_CameraState) {
                m_CameraState = value;
                ApplyPosition();
            }
        }
    }
    public CameraDict CameraPositions;
    public CameraData CurrentCameraPosition {
        get {
            return CameraPositions[CameraState];
        }
    }

    [Header("Core")]
    public Camera GameCamera;
    public Camera CanvasCamera;
    [SerializeField]
    private Transform m_Holder;
    [SerializeField]
    private Transform m_Rotator;
    [SerializeField]
    private Transform m_Hand;
    [SerializeField]
    private Transform m_Zoom;

#if UNITY_EDITOR
    [Button]
    public void SetRefs() {
        GameObject _goGameCamera = GameObject.Find("Game Camera");
        GameObject _goCanvasCamera = GameObject.Find("Canvas Camera");

        if (_goGameCamera == null) {
            Debug.LogError("There is no GameObject with name \"Game Camera\" ");
        }
        else {
            GameCamera = _goGameCamera.GetComponent<Camera>();
        }

        if (_goCanvasCamera == null) {
            Debug.LogError("There is no GameObject with name \"Canvas Camera\" ");
        }
        else {
            CanvasCamera = _goCanvasCamera.GetComponent<Camera>();
        }

        m_Holder = transform;
        m_Hand = transform.GetChild(0);
        m_Rotator = transform.GetChild(0).GetChild(0);
        m_Zoom = transform.GetChild(0).GetChild(0).GetChild(0);
    }

    [Header("Add/Refresh Camera Position")]
    [PropertyOrder(-1)]
    [Tooltip("Add or refresh camera position data with current position and rotation")]
    public CameraStates state;
    [Button("Add/Refresh Camera Position")]
    [PropertyOrder(-1)]
    public void RefreshCameraReference() {
        if (CameraPositions.ContainsKey(state)) {
            CameraPositions.TryGetValue(state, out CameraData _data);
            _data.Position = GameCamera.transform.position;
            _data.Rotation = GameCamera.transform.rotation.eulerAngles;
        }
        else {
            CameraData _data = new CameraData();
            _data.Position = GameCamera.transform.position;
            _data.Rotation = GameCamera.transform.rotation.eulerAngles;
            CameraPositions.Add(state, _data);
        }
    }
#endif

    [Header("Follow Target")]
    public Transform Target;
    [SerializeField]
    private float m_Smoothness;

    [Header("Zoom")]
    [SerializeField]
    private float LevelZoomCoeffiecient = 2f;

    [Header("Shake")]
    public bool IsShakeAllowed = true;
#if UNITY_EDITOR
    [ShowIf("IsShakeAllowed")]
#endif
    public CameraShakeData ShakeData;


    private void OnEnable() {
        GameManager.OnLevelLoaded += UpdateLevelZoom;
        ApplyPosition();
        UpdateLevelZoom(0);

    }

    public override void OnDisable() {
        base.OnDisable();
    }

    private void FixedUpdate() {
        if (GameManager.Instance.GameState != GameStates.GameComplete && Target != null) {
            Vector3 pos = Target.position;
            m_Holder.position = Vector3.Lerp(m_Holder.position, pos, m_Smoothness * Time.fixedDeltaTime);
        }
    }

    private void ApplyPosition() {
        if (!CameraPositions.ContainsKey(CameraState)) return;
        m_Rotator.DOKill();
        m_Hand.DOKill();
        m_Rotator.DOLocalRotate(CurrentCameraPosition.Rotation, CurrentCameraPosition.TweenDuration);
        m_Hand.DOLocalMove(CurrentCameraPosition.Position, CurrentCameraPosition.TweenDuration);

    }

    public void UpdateLevelZoom(int i_Level) {
        m_Zoom.DOLocalMove(Vector3.forward * i_Level * LevelZoomCoeffiecient, 1f);
    }
    public void UpdateLevelZoom() {
        m_Zoom.DOKill();
        m_Zoom.DOLocalMove(Vector3.forward * LevelZoomCoeffiecient, 1f);
    }



    public void Shake(float i_Magnitude = 0f) {
        DoShakeCoroutine(ShakeData, i_Magnitude);
    }

    private Coroutine m_ShakeCoroutine;
    public void DoShakeCoroutine(CameraShakeData i_Shake, float i_PositionMagnitude = 0f) {
        if (i_PositionMagnitude != 0f) {
            ShakeData.PositionMagnitude = i_PositionMagnitude;
        }
        if (!IsShakeAllowed) {
            return;
        }
        if (m_ShakeCoroutine != null) {
            //StopCoroutine(m_ShakeCoroutine);
            return;
        }
        else {
            m_ShakeCoroutine = StartCoroutine(IEShakeCoroutine(i_Shake));
        }
    }
    private float m_TimeLeft;
    private float m_PositionShakeMagnitude;
    private float m_RotationShakeMagnitude;

    private float m_ShakeX;
    private float m_ShakeY;
    private float m_ShakeZ;


    IEnumerator IEShakeCoroutine(CameraShakeData i_Shake) {
        if (i_Shake.Delay > 0) {
            yield return new WaitForSeconds(i_Shake.Delay);
        }
        m_TimeLeft = i_Shake.Durration;
        GameCamera.transform.localPosition = Vector3.zero;
        GameCamera.transform.localEulerAngles = Vector3.zero;
        while (m_TimeLeft > 0 && IsShakeAllowed) {
            if (i_Shake.ShakePosition) {
                m_PositionShakeMagnitude = i_Shake.EaseCurve.Evaluate(m_TimeLeft / i_Shake.Durration) * i_Shake.PositionMagnitude;
                m_ShakeX = m_PositionShakeMagnitude * UnityEngine.Random.Range(-1f, 1f) * i_Shake.PositionAxis.x;
                m_ShakeY = m_PositionShakeMagnitude * UnityEngine.Random.Range(-1f, 1f) * i_Shake.PositionAxis.y;
                m_ShakeZ = m_PositionShakeMagnitude * UnityEngine.Random.Range(-1f, 1f) * i_Shake.PositionAxis.z;
                GameCamera.transform.localPosition = new Vector3(m_ShakeX, m_ShakeY, m_ShakeZ);
            }
            if (i_Shake.ShakeRotation) {
                m_RotationShakeMagnitude = i_Shake.EaseCurve.Evaluate(m_TimeLeft / i_Shake.Durration) * i_Shake.RotationMagnitude;
                m_ShakeX = m_RotationShakeMagnitude * UnityEngine.Random.Range(-1f, 1f) * i_Shake.RotationAxis.x;
                m_ShakeY = m_RotationShakeMagnitude * UnityEngine.Random.Range(-1f, 1f) * i_Shake.RotationAxis.y;
                m_ShakeZ = m_RotationShakeMagnitude * UnityEngine.Random.Range(-1f, 1f) * i_Shake.RotationAxis.z;
                GameCamera.transform.localEulerAngles = new Vector3(m_ShakeX, m_ShakeY, m_ShakeZ);
            }
            yield return new WaitForEndOfFrame();
            m_TimeLeft -= Time.deltaTime;
        }
        yield return null;
        GameCamera.transform.localPosition = Vector3.zero;
        GameCamera.transform.localEulerAngles = Vector3.zero;
        m_ShakeCoroutine = null;
    }


}
[System.Serializable]
public class CameraShakeData {
    public float Delay = 0;
    public float PositionMagnitude = .1f;
    public float RotationMagnitude = 5f;
    public float Durration = .2f;
    public bool ShakeRotation;
    public bool ShakePosition = true;
    public Vector3 PositionAxis = Vector3.right + Vector3.up;
    public Vector3 RotationAxis = Vector3.one;

    public AnimationCurve EaseCurve;
}
[System.Serializable]
public class CameraData {
    public Vector3 Position;
    public Vector3 Rotation;
    public float TweenDuration = 1f;
}
[System.Serializable]
public class CameraDict : UnitySerializedDictionary<CameraStates, CameraData> {

}