﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
[DefaultExecutionOrder(-50)]
public class GameManager : Singleton<GameManager> {
    #region Game Events
    public delegate void GameEvent();
    public static event GameEvent OnGameStateChange = delegate { };
    public static event GameEvent OnLevelLoaded = delegate { };
    public static event GameEvent OnLevelComplete = delegate { };
    public static event GameEvent OnLevelFailed = delegate { };
    public static event GameEvent OnLevelStarted = delegate { };
    #endregion
    public GameStates GameState {
        get {
            return m_GameState;
        }
        set {
            if (value != m_GameState) {
                m_GameState = value;
                OnGameStateChange?.Invoke();
            }

        }
    }
    private GameStates m_GameState;

    public Character Player;

    protected override void OnAwakeEvent() {
        Application.targetFrameRate = 60;
    }

    public void StartLevel() {
        GameState = GameStates.Playing;
        CameraManager.Instance.CameraState = CameraStates.Start;
        OnLevelStarted?.Invoke();
    }

    public void LevelCompleted() {
        GameState = GameStates.GameComplete;
        CameraManager.Instance.CameraState = CameraStates.WinLevel;
        Debug.Log("LevelComplete");
        OnLevelComplete?.Invoke();
    }
    public void LevelFailed() {
        GameState = GameStates.GameOver;
        CameraManager.Instance.CameraState = CameraStates.FailLevel;
        OnLevelFailed?.Invoke();
    }

    public void LoadLevel() {


    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(0);
    }


}