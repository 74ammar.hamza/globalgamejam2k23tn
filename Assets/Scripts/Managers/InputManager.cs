﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using Sirenix.OdinInspector;

public class InputManager : Singleton<InputManager>
{
    [SerializeField]
    private float m_Sensitivity = 1F;

    public Vector3 MouseWorldPosition;
    [SerializeField]
    private LayerMask m_LayerMask;
    private Vector3 m_Drag;
    public Vector3 Drag
    {
        get
        {
            return m_Drag * m_Sensitivity;
        }
    }
    [ShowInInspector]
    public Vector3 RatioDrag
    {
        get
        {
            return (m_DeltaDrag / Screen.width) /.8f;
        }
    }
    private Vector3 m_DampedDrag;
    public Vector3 DampedDrag
    {
        get
        {
            return m_DampedDrag* m_Sensitivity;
        }
    }
    public Vector3 DeltaDrag
    {
        get
        {
            return m_DeltaDrag * m_Sensitivity;
        }
    }
    private Vector3 m_DeltaDrag;

    private Vector3 m_LastInputPosition;


    public bool IsInputDown
    {
        get
        {
            return Input.GetMouseButton(0);
        }
    }
    public Vector3 TouchPosition
    {
        get
        {
            return Input.mousePosition;
        }
    }

    private Vector3 m_InputDownPosition;

    private Ray m_Ray;
    private RaycastHit m_Hit;
    private void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            //GameManager.Instance.LoadLevel();
        }
        if (Input.GetMouseButtonDown(0))
        {
            m_InputDownPosition = TouchPosition;
            m_LastInputPosition = TouchPosition;
        }

        if (IsInputDown)
        {
            m_Drag = TouchPosition - m_InputDownPosition;
            m_DeltaDrag = TouchPosition - m_LastInputPosition;
            m_DampedDrag += DeltaDrag;
            m_LastInputPosition = TouchPosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            m_DeltaDrag = Vector3.zero;
        }
        m_Drag *= .9f;
        m_DampedDrag *= .9f;
        m_Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(m_Ray,out m_Hit, Mathf.Infinity, m_LayerMask))
        {
            MouseWorldPosition = m_Hit.point;
        }
    }

}

