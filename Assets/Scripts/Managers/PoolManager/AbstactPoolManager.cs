﻿using System.Collections.Generic;
using System;
using UnityEngine;
namespace ManagerPooling
{
    public static class AbstactPoolManager<T>
    {
        public static PoolDict AllPools = new PoolDict();

        public static void PoolInstaller(ePoolItem idGroup, List<GameObject> objectsForPool)
        {
            var newPool = new Pool(objectsForPool);
            AllPools.Add(idGroup, newPool);
        }
        public static GameObject GetFromPool(ePoolItem idGroup,T itemForPool, Func<GameObject> createObjectFunc)
        {
            if (AllPools.ContainsKey(idGroup))
            {
                var objectFromPool = AllPools[idGroup].GetFromPool();
                return objectFromPool;
            }
            else
            {
                var newPool = new Pool(createObjectFunc,1);
                AllPools.Add(idGroup, newPool);
                return AllPools[idGroup].GetFromPool();
            }
        }

        public static void BackToPool(ePoolItem idGroup, GameObject item)
        {
            if (AllPools.ContainsKey(idGroup) && AllPools[idGroup].ObjectsInPool.ContainsKey(item))
            {
                AllPools[idGroup].ObjectsInPool[item].Free = true;
            }
        }
    }
}
