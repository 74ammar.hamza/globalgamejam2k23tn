using UnityEngine;

    [System.Serializable]
    public class ItemPool
    {
        public GameObject ItemObject;
        public bool Free = true;
    }

