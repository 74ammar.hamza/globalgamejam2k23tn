using System.Collections.Generic;
using System;
using UnityEngine;

    [System.Serializable]
    public class Pool
    {
        public ObjectsDict ObjectsInPool = new ObjectsDict();


        public Func<GameObject> func;
        public Pool(Func<GameObject> inputObject,int size)
        {
            func = inputObject;
            ObjectsInPool = new ObjectsDict();
            for (int i = 0; i < size; i++)
            {
                CreateNewObject();
            }
        }

        public Pool(List<GameObject> objectsPool)
        {
            foreach (var item in objectsPool)
            {
                var newObject = new ItemPool();
                newObject.ItemObject = item;
                ObjectsInPool.Add(newObject.ItemObject, newObject);
            }
        }

        public GameObject GetFromPool(Func<GameObject> i_func = null)
        {
            foreach (var item in ObjectsInPool)
            {
                if (item.Value.Free)
                {
                    item.Value.Free = false;
                    return item.Value.ItemObject;
                }
            }
            return CreateNewObject(i_func);
        }

        public void BackToPool(GameObject item)
        {
            if (ObjectsInPool.ContainsKey(item))
            {
                ObjectsInPool[item].Free = true;
            }
        }

        private GameObject CreateNewObject(Func<GameObject> i_func = null)
        {
            var newObject = new ItemPool();
            if (func != null)
            {
                newObject.ItemObject = func();

            }
            else
            {
                newObject.ItemObject = i_func();
            }
            ObjectsInPool.Add(newObject.ItemObject, newObject);
            return newObject.ItemObject;
        }
    }
[System.Serializable]
public class ObjectsDict : UnitySerializedDictionary<GameObject, ItemPool>
{

}