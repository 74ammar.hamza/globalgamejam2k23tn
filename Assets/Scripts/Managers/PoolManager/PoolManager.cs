﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using Sirenix.OdinInspector;
using UnityEditor;
#endif

    public class PoolManager : Singleton<PoolManager>
    {
        [SerializeField]
        private PoolItemDefinitions PoolItems = new PoolItemDefinitions();
        [SerializeField]
        public PoolDict AllPools = new PoolDict();
        [SerializeField]
        private Transform m_Holder;

        public enum SceneType
        {
            OnceLoad, DontDestroy
        }
        public SceneType TypeLoad;


        protected override void OnAwakeEvent()
        {
            base.OnAwakeEvent();
            SetTypeManager();
        }

        private void SetTypeManager()
        {
            switch (TypeLoad)
            {
                case SceneType.OnceLoad: { break; }
                case SceneType.DontDestroy:
                    {
                        if (!Instance)
                            DontDestroyOnLoad(this);
                        else Destroy(this);
                        break;
                    }
            }
        }
#if UNITY_EDITOR
        [Button]
#endif
        public void Prebake()
    {
            AllPools = new PoolDict();
            if (m_Holder != null) {
                DestroyImmediate(m_Holder.gameObject);
                    }
            m_Holder = new GameObject("PoolHolder").transform;
            m_Holder.parent = transform;
            foreach (KeyValuePair<ePoolItem, PoolDefinition> poolItem in PoolItems)
            {
                PoolInstaller(poolItem.Value.Prefab, poolItem.Value.Quantity, poolItem.Key);
            }
        }
        public void PoolInstaller(GameObject prefab, int size, ePoolItem idGroup)
        {
            var rootTransform = new GameObject();
            rootTransform.name = prefab.name + " Pool";
            rootTransform.transform.parent = m_Holder;
            var pool = new Pool(() => { return InstantiateObject(prefab, rootTransform); }, size);
            AllPools.Add(idGroup, pool);
        }

        public GameObject SpawnObject(ePoolItem idGroup)
        {
            if (AllPools.ContainsKey(idGroup))
            {
                var objectFromPool = AllPools[idGroup].GetFromPool(()=> { return InstantiateObject(PoolItems[idGroup].Prefab, m_Holder.gameObject); });
                objectFromPool.SetActive(true);
                return objectFromPool;
            }
            else
            {
                PoolInstaller(PoolItems[idGroup].Prefab, 1, idGroup);
                //Debug.Log("This Item Was not Pooled");
                return null;
            }
        }

        public void BackToPool(GameObject item, ePoolItem idGroup)
        {
            if (AllPools.ContainsKey(idGroup))
            {
                item.SetActive(false);
                AllPools[idGroup].BackToPool(item);
            }
        }

        private GameObject InstantiateObject(GameObject prefab, GameObject rootTransform)
        {
            GameObject newObject = null;
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                newObject = Instantiate(prefab) as GameObject;
            }
            else
            {
                newObject = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
            }
#else
            newObject = Instantiate(prefab) as GameObject;
#endif
            if (newObject == null)
            {
                Debug.LogError("You should reference the Prefab from the Asset Menu and not from the Scene hierarchy!");
                return null;
            }
            newObject.transform.SetParent(rootTransform.transform);
            newObject.SetActive(false);
            return newObject;
        }
    }

[System.Serializable]
public class PoolDict : UnitySerializedDictionary<ePoolItem, Pool>
{

}
[System.Serializable]
public class PoolItemDefinitions : UnitySerializedDictionary<ePoolItem, PoolDefinition>
{

}

[System.Serializable]
public class PoolDefinition
{
    public GameObject Prefab;
    public int Quantity;
 
}
//PoolInstaller