﻿/*
* Copyright (c) Kiwert 2021
*/

using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    private void Start() {
        SceneManager.LoadScene(1);
    }
}