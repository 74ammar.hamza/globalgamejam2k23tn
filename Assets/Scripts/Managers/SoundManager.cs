/*
* Copyright (c) Kiwert 2022
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class SoundManager : Singleton<SoundManager>
{
    public AudioSource MusicSource;
    [SerializeField]
    private DictionarySounds SoundEffects;
    public void PlaySoundFX(eSounds i_Sound)
    {
            if (SoundEffects.ContainsKey(i_Sound))
            {
            MusicSource.PlayOneShot(SoundEffects[i_Sound].AudioClip, SoundEffects[i_Sound].Volume);
            }
    }

}

[Serializable]
public class SoundData
{
    public AudioClip AudioClip;
    [Range(0f, 1f)]
    public float Volume = .5f;

}
[Serializable]
public class DictionarySounds : UnitySerializedDictionary<eSounds, SoundData>
{
}
