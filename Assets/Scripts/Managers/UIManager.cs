﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
[DefaultExecutionOrder(-45)]
public class UIManager : Singleton<UIManager>
{
    protected override void OnAwakeEvent() 
    {

    }
    private float HpValue;
    private float WaterValue;
    [SerializeField]
    private Slider m_SliderHP;

    [SerializeField]
    private Slider m_SliderWaterCooldown;
    [SerializeField]
    private TMP_Text m_Tasks;
    private int m_TaskCount = 0;
    private void OnEnable()
    {
        HpValue = 1f;
        WaterValue = 1f;
        m_SliderHP.value = 1f;
        m_SliderWaterCooldown.value = 1f;
        m_TaskCount = 0;
        m_Tasks.text = "0/3";
        Roots.OnRootFinished += AddTask;
    }
    public override void OnDisable()
    {
        base.OnDisable();
        Roots.OnRootFinished -= AddTask;

    }

    public void AddTask()
    {
        m_TaskCount++;
        m_Tasks.text = m_TaskCount.ToString() + "/3";
        if (m_TaskCount == 3)
        {
            GameManager.Instance.ReloadLevel();
        }
    }

    public void SetHpValue(float i_Value)
    {
        HpValue = i_Value;
    }

    public void SetWaterValue(float i_Value)
    {
        WaterValue = i_Value;
    }

    private void Update()
    {
        m_SliderHP.value = Mathf.Lerp(m_SliderHP.value, HpValue, 10 * Time.deltaTime);
        m_SliderWaterCooldown.value = Mathf.Lerp(m_SliderWaterCooldown.value, WaterValue, 10 * Time.deltaTime);
    }

}