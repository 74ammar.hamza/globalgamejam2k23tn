﻿/*
* Copyright (c) Kiwert 2021
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
#if UNITY_EDITOR
using Sirenix.OdinInspector;
#endif

public class Projectile : MonoBehaviour
{
    #region Action Events
    #endregion

    #region Public Variables
    public float ProjectileSpeed=10f;
    public float ProjectileSpeedOnObstacles=30f;
    public float ProjectilSpeedOnEnnemy = 10f;
    #endregion

    #region Private Variables
    [SerializeField]
    private Transform m_Transform;
    [SerializeField]
    private Transform m_TrailHolder;
    [SerializeField]
    private ParticleSystem[] m_Trail;
    [SerializeField]
    private Transform m_ExplosionHolder;
    [SerializeField]
    private ParticleSystem[] m_ExplosionFX;
    [SerializeField]
    private Transform m_MuzzleHolder;
    [SerializeField]
    private ParticleSystem[] m_MuzzleFx;
    [SerializeField]
    private float m_ExplosionDuratin = 2f;
    [SerializeField]
    private float m_Speed = 5f;
    [SerializeField]
    private float m_LifeTime = 4f;
    [SerializeField]
    private Vector3 m_Direction;
    private bool m_HasExploded = false;
    [SerializeField]
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private float m_Damage = 1;
    [SerializeField]
    private CharacterBase m_Emitter;
    #endregion	
	#region Unity Methods
    void Start()
    {
        
    }


    void FixedUpdate()
    {
        if (!m_HasExploded)
        {
            m_Rigidbody.MovePosition(transform.position + m_Direction * m_Speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Ennemy"))
        {
            CharacterBase targetHit = other.GetComponent<CharacterBase>();
            if (targetHit != null && targetHit != m_Emitter) 
            {
                Explode(targetHit);
            }
        }
    }
    public void LaunchProjectile(CharacterBase i_emitter, Vector3 i_Direction)
    {
        m_Emitter = i_emitter;
        m_Direction = i_Direction;
        m_HasExploded = false;
        PlayMuzzle();
        PlayTrail();

    }

    public void Explode(CharacterBase i_CharacterBase)
    {
        m_HasExploded = true;
        StopTrail();
        SoundManager.Instance.PlaySoundFX(eSounds.Explosion);
        i_CharacterBase.RecieveDamage(m_Damage);
        if (m_ExplosionFX != null)
        {
            PlayExplosion();
            Invoke(nameof(Dequeue), m_ExplosionDuratin);
        }
        else
        {
            Dequeue();  
        }
    }


    public void Dequeue()
    {
        PoolManager.Instance.BackToPool(gameObject, ePoolItem.Item);
    }


    public void PlayTrail()
    {
        PlayMuzzle();
        for (int i = 0; i< m_Trail.Length; i++)
        {
            m_Trail[i].Play();
        }
    }
    public void StopTrail()
    {
        for (int i = 0; i < m_Trail.Length; i++)
        {
            m_Trail[i].Stop();
        }

    }

    
    public void PlayExplosion()
    {
        for (int i = 0; i< m_ExplosionFX.Length; i++)
        {
            m_ExplosionFX[i].Play();
        }
        StopTrail();
        CameraManager.Instance.Shake();
    }

    public void PlayMuzzle()
    {
        for (int i = 0; i< m_MuzzleFx.Length; i++)
        {
            m_MuzzleFx[i].Play();
        }
    }

#if UNITY_EDITOR
    [Button]
    public void SetRefs()
    {
        m_Transform = transform;
        m_TrailHolder = m_Transform.GetChild(0);
        m_TrailHolder.localPosition = Vector3.zero;
        m_ExplosionHolder = m_Transform.GetChild(1);
        m_ExplosionHolder.localPosition = Vector3.zero;
        m_Trail = m_TrailHolder.GetComponentsInChildren<ParticleSystem>();
        foreach (var comp in m_TrailHolder.GetComponents<Component>())
        {
            if (!(comp is Transform))
            {
                DestroyImmediate(comp);
            }
        }
        m_ExplosionFX = m_ExplosionHolder.GetComponentsInChildren<ParticleSystem>();
        foreach (var comp in m_ExplosionHolder.GetComponents<Component>())
        {
            if (!(comp is Transform))
            {
                DestroyImmediate(comp);
            }
        }
        if (m_Transform.childCount >= 2)
        {
            m_MuzzleHolder = m_Transform.GetChild(2);
            m_MuzzleHolder.transform.localPosition = Vector3.zero;
            m_MuzzleFx = m_MuzzleHolder.GetComponentsInChildren<ParticleSystem>();
            foreach (var comp in m_MuzzleHolder.GetComponents<Component>())
            {
                if (!(comp is Transform))
                {
                    DestroyImmediate(comp);
                }
            }

        }
        var particles = GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i< particles.Length; i++)
        {
            var main = particles[i].main;
            main.playOnAwake = false;
            particles[i].playOnAwake = false;
        }
        for (int i=0; i< m_MuzzleFx.Length; i++)
        {
            m_MuzzleFx[i].simulationSpace = ParticleSystemSimulationSpace.World;
        }
    }
    [Button]
    public void SetScalingToHierarchy()
    {
        for (int i = 0; i< m_Trail.Length; i++)
        {
            m_Trail[i].scalingMode = ParticleSystemScalingMode.Hierarchy;
        }
        for (int i = 0; i< m_ExplosionFX.Length; i++)
        {
            m_ExplosionFX[i].scalingMode = ParticleSystemScalingMode.Hierarchy;
        }
        for (int i = 0; i< m_MuzzleFx.Length; i++)
        {
            m_MuzzleFx[i].scalingMode = ParticleSystemScalingMode.Hierarchy;
        }
    }

#endif
#endregion
}
