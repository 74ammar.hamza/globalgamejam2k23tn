using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBase : MonoBehaviour
{
    public virtual void LaunchProjectile()
    {

    }

    public virtual void PlayExplosion()
    {

    }
}
