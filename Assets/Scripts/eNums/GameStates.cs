﻿public enum GameStates {
    Idle,
    Playing,
    Paused,
    GameOver,
    GameComplete,
    Reward,
}