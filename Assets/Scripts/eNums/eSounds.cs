public enum eSounds
{
    Sound,
    walk1,
    walk2,
    walk3,
    walk4,
    walk5,
    walk6,
    walk7,
    walk8,
    walk9,
    walk10,
    jump_start,
    jump_land,
    grapple,
    Explosion,
    WaterPump,
    Success,

}