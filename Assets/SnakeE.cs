using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeE : MonoBehaviour
{
 
    public Transform[] patrolPoints;
    public float speed = 5.0f;
    public float chaseSpeed = 10.0f;
    public float chaseDistance = 10.0f;
   


    private int currentPatrolPoint = 0;
    private Transform player;
    public float attackRange = 5f;
    private bool isChasing = false;
    private Animator animator;



    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player").transform;

        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);
        if (!isChasing && distanceToPlayer <= chaseDistance)
        {
            isChasing = true;
        }
        else if (isChasing && distanceToPlayer > attackRange)
        {
            ChasePlayer();
            
      
        }
        else if (isChasing && distanceToPlayer <= attackRange)
        {
            isChasing = false;
            StopChasing();
            Attack();
         
        }
        else
        {
            Patrol();
        }
      
    }
    void Patrol()
    {
        transform.position = Vector3.MoveTowards(transform.position, patrolPoints[currentPatrolPoint].position, speed * Time.deltaTime);
        transform.LookAt(patrolPoints[currentPatrolPoint]);

        animator.SetBool("Walk", true);

        if (Vector3.Distance(transform.position, patrolPoints[currentPatrolPoint].position) < 0.1f)
        {
            currentPatrolPoint = (currentPatrolPoint + 1) % patrolPoints.Length;
        }
    }
    void ChasePlayer()
    {
        Vector3 targetPosition = player.position;
        float playerY = player.position.y;
        float enemyY = transform.position.y;
        if (playerY - enemyY > 0f) // player is higher than the enemy by more than 1 unit
        {
            targetPosition.y = enemyY; // enemy does not go up
        }
        animator.SetBool("Walk", true);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, chaseSpeed * Time.deltaTime);


        Vector3 direction = player.position - transform.position;
        direction.y = 0;
        direction = direction.normalized;
        transform.rotation = Quaternion.LookRotation(direction);
        
    }
    void StopChasing()
    {

    }
    void Attack()
    {
        animator.SetBool("Walk", false);
        animator.SetTrigger("Attack");




    }
    public void Die()
    {
        animator.SetTrigger("Dead 0");
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            // The projectile has made contact with an object with the "Enemy" tag
            // Destroying the enemy
            Die();
        }
    }
   
    
}
