using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderE : CharacterBase
{
    public GameObject projectilePrefab;
    public float speed = 5.0f;
    public float chaseSpeed = 10.0f;
    public float chaseDistance = 10.0f;
     public Transform shootPoint;
    public float shootForce = 10f;
    public float shootDelay = 2f;
    private bool currentPatrolPoint;
    private Transform player
    {
        get
        {
            return GameManager.Instance.Player.transform;
        }
    }
    public float attackRange = 5f;
    private bool isChasing = false;
    private Animator animator;
    public float spawnTime;
    public bool stopSpawning = false;
      public float sphereRadius;

    private bool functionCalled = false;

    private Vector3 m_InitialPosition;
    private float m_RoamingDistance = 5f;
    private Vector3 m_PatrolPoint
    {
        get
        {
            return m_InitialPosition + (currentPatrolPoint ? m_RoamingDistance : -m_RoamingDistance) * Vector3.right;
        }
    }
    private void OnEnable()
    {
        m_InitialPosition = transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {

        animator = GetComponent<Animator>();
     
    }

    // Update is called once per frame
    void Update()
    {
        if (CurrentHealth <= 0) return;
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);
        if (!isChasing && distanceToPlayer <= chaseDistance)
        {
            isChasing = true;
        }
        else if (isChasing && distanceToPlayer > attackRange)
        {
            ChasePlayer();
            CancelInvoke("ShootProjectile");
            functionCalled = false;
        }
        else if (isChasing && distanceToPlayer <= attackRange)
        {
            isChasing = false;
            StopChasing();
            Attack();
            if (!functionCalled)
            {
                functionCalled = true;
               // InvokeRepeating("ShootProjectile", spawnTime, shootDelay);
            }
        }
        else
        {
            Patrol();
        }
        NoSpawn();
    }
    void Patrol()
    {
        transform.position = Vector3.MoveTowards(transform.position, m_PatrolPoint, speed * Time.deltaTime);
        transform.LookAt(m_PatrolPoint);

        animator.SetBool("walk",true);

        if (Vector3.Distance(transform.position, m_PatrolPoint) < 0.1f)
        {
            currentPatrolPoint = !currentPatrolPoint;
        }
    }
    void ChasePlayer()
    {
        Vector3 targetPosition = player.position;
        float playerY = player.position.y;
        float enemyY = transform.position.y;

        if (playerY - enemyY > 0f) 
        {
            targetPosition.y = enemyY; 
        }
        animator.SetBool("walk", true);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, chaseSpeed * Time.deltaTime);
        transform.LookAt(player);
    }
    void StopChasing()
    {
        
    }
    void Attack()
    {
        animator.SetBool("walk", false);
        animator.SetTrigger("Attack");

       


    }

    public override void RecieveDamage(float i_Damage)
    {
        base.RecieveDamage(i_Damage);
    }
    public override void Die()
    {
        animator.SetBool("Dead",true);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            // The projectile has made contact with an object with the "Enemy" tag
            // Destroying the enemy
            Die();
        }
    }
    void ShootProjectile()
    {

        Vector3 direction = (player.transform.position - shootPoint.position).normalized;
        direction.z = 0;
        direction = direction.normalized;

        Projectile projectile = PoolManager.Instance.SpawnObject(ePoolItem.Item).GetComponent<Projectile>();
        projectile.transform.position = shootPoint.position;
        projectile.LaunchProjectile(this as CharacterBase, direction);
    }
    public void NoSpawn()
    {
        // stop the spawning of item
        if (Physics.CheckSphere(transform.position, sphereRadius))
        {
            stopSpawning = true;
        }
        else
        {
            stopSpawning = false;

        }

    }

}
