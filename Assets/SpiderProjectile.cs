using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderProjectile : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform shootPoint;
    public float shootForce;
    public bool stopSpawning = false;
    private GameObject player;
    public float spawnTime;
    public float shootDelay;
    public float sphereRadius;
    public static SpiderProjectile Instance { get; set; }
    void Start()

    {
        InvokeRepeating("ShootProjectile", spawnTime, shootDelay);
        player = GameObject.FindWithTag("Player");
    }
    void Update()
    {


        NoSpawn();

    }
    public void ShootProjectile()
    {
        Vector3 direction = (player.transform.position - shootPoint.position).normalized;
        GameObject projectile = Instantiate(projectilePrefab, shootPoint.position, Quaternion.identity);
        Rigidbody rigidbody = projectile.GetComponent<Rigidbody>();
        rigidbody.AddForce(direction * shootForce, ForceMode.Impulse);
    }
    public void NoSpawn()
    {
        // stop the spawning of item
        if (Physics.CheckSphere(transform.position, sphereRadius))
        {
            stopSpawning = true;
        }
        else
        {
            stopSpawning = false;

        }

    }

    public void ShootProjectile ( CharacterInfo i_Character)
    {

    }

    public void ShootProjectile ( Vector3 i_Direction)
    {

    }

    //void Update()
    //{
    //    if (player != null)
    //    {
    //        Vector3 direction = (player.transform.position - shootPoint.position).normalized;
    //        GameObject projectile = Instantiate(projectilePrefab, shootPoint.position, Quaternion.identity);
    //        Rigidbody rigidbody = projectile.GetComponent<Rigidbody>();
    //        rigidbody.AddForce(direction * projectileSpeed, ForceMode.Impulse);
    //    }
    //}
}
