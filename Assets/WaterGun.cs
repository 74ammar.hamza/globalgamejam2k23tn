using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGun : MonoBehaviour
{
    private void OnParticleCollision(GameObject other)
    {
        Debug.Log(other.tag);
        if (other.CompareTag("Root"))
        {
            Roots root = other.GetComponent<Roots>();
            root.Grow();
        }
        else if (other.CompareTag("Ennemy"))
        {
            CharacterBase ennemy = other.GetComponent<CharacterBase>();
            ennemy.RecieveDamage(.1f);
        }
    }

    private void LateUpdate()
    {
        Vector3 position = transform.eulerAngles;
        position.x = 0;
        position.y = 0;
        transform.eulerAngles = position;
    }
}
